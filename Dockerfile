# ASP.NET Core Runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS base
WORKDIR /app
EXPOSE $PORT

# Restore and Build
FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-web
WORKDIR /src
COPY ["src/Web/Web.csproj", "Web/"]
RUN dotnet restore "Web/Web.csproj"
COPY ["src/Web/.", "Web/"]
WORKDIR /src/Web
RUN dotnet build "Web.csproj" -c Release -o /app

# Publish
FROM build-web AS publish-web
RUN dotnet publish "Web.csproj" -c Release -o /app

# Restore and Build
FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-cli
WORKDIR /src
COPY ["src/Cli/Cli.csproj", "Cli/"]
RUN dotnet restore "Cli/Cli.csproj"
COPY ["src/Cli/.", "Cli/"]
WORKDIR /src/Cli
RUN dotnet build "Cli.csproj" -c Release -o /app

# Publish
FROM build-cli AS publish-cli
RUN dotnet publish "Cli.csproj" -c Release -o /app

# Final runtime image
FROM base AS final
WORKDIR /app
COPY --from=publish-web /app .
COPY --from=publish-cli /app cli/.
ENV ASPNETCORE_HTTPS_PORT 443
CMD ASPNETCORE_URLS=http://*:$PORT dotnet Web.dll --server.urls "http://*:$PORT"
