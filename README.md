<!-- markdownlint-disable MD003 -->
<!-- markdownlint-disable MD024 -->
<!-- markdownlint-disable MD036 -->

Wipfli Tech Services: Dotnet Core on Heroku using Docker
========================================================

Commands
--------

### Web container

- Build web container:  
`> docker build -t wipflits/heroku-docker-spike-web .\src\Web\.`
- Run web container (locally):  
`> docker run --rm -it -p 5000:5000 -e PORT=5000 --name heroku-docker-spike-web wipflits/heroku-docker-spike-web`

### CLI container

- Build CLI container:  
`> docker build -t wipflits/heroku-docker-spike-cli .\src\Cli\.`
- Run CLI container (locally):  
`> docker run --rm -it --name heroku-docker-spike-cli wipflits/heroku-docker-spike-cli`

### Combined Web+CLI container

- Build the combined container:  
`> docker build -t wipflits/heroku-docker-spike .`
- Run combined container (locally):  
`> docker run --rm -it -p 8080:8080 -e PORT=8080 --name heroku-docker-spike wipflits/heroku-docker-spike`
- Run CLI in combined container (locally):  
`> docker exec -i -t heroku-docker-spike dotnet cli/Cli.dll`

### Heroku

- Deploy to Heroku:  
`> git push heroku master`
- Run CLI in Heroku (one-off/scheduler):  
`> heroku run dotnet cli/Cli.dll`
