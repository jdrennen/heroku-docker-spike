﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Wipfli.HerokuDockerSpike.Web {
	public class Startup {
		public Startup(IConfiguration configuration) {
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services) {
			services.Configure<CookiePolicyOptions>(options => {
				// This lambda determines whether user consent for non-essential cookies is needed for a given request.
				options.CheckConsentNeeded = context => true;
				options.MinimumSameSitePolicy = SameSiteMode.None;
			});


			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

			// services.Configure<ForwardedHeadersOptions>(options => {
			// 	options.ForwardedHeaders =
			// 	ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
			// });
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
			ForwardedHeadersOptions options = new ForwardedHeadersOptions {
				ForwardedHeaders = ForwardedHeaders.XForwardedProto | ForwardedHeaders.XForwardedFor
			};
			// Temporary Workaround. see https://github.com/aspnet/AspNetCore.Docs/issues/2384
			options.KnownNetworks.Clear();
			options.KnownProxies.Clear();
			app.UseForwardedHeaders(options);

			if (env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();
			} else {
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseStaticFiles();
			app.UseCookiePolicy();

			app.UseMvc(routes => {
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");
			});
			// WriteHeadersToResponse(app);
		}

		// private void LogHeaders(IApplicationBuilder app) {
		// 	var logger = _loggerFactory.CreateLogger<Startup>();

		// 	app.Use(async (context, next) => {
		// 		// Request method, scheme, and path
		// 		logger.LogDebug("Request Method: {METHOD}", context.Request.Method);
		// 		logger.LogDebug("Request Scheme: {SCHEME}", context.Request.Scheme);
		// 		logger.LogDebug("Request Path: {PATH}", context.Request.Path);

		// 		// Headers
		// 		foreach (var header in context.Request.Headers) {
		// 			logger.LogDebug("Header: {KEY}: {VALUE}", header.Key, header.Value);
		// 		}

		// 		// Connection: RemoteIp
		// 		logger.LogDebug("Request RemoteIp: {REMOTE_IP_ADDRESS}",
		// 			context.Connection.RemoteIpAddress);

		// 		await next();
		// 	});
		// }

		private void WriteHeadersToResponse(IApplicationBuilder app) {
			app.Run(async (context) => {
				context.Response.ContentType = "text/plain";

				// Request method, scheme, and path
				await context.Response.WriteAsync(
					$"Request Method: {context.Request.Method}{Environment.NewLine}");
				await context.Response.WriteAsync(
					$"Request Scheme: {context.Request.Scheme}{Environment.NewLine}");
				await context.Response.WriteAsync(
					$"Request Path: {context.Request.Path}{Environment.NewLine}");

				// Headers
				await context.Response.WriteAsync($"Request Headers:{Environment.NewLine}");

				foreach (var header in context.Request.Headers) {
					await context.Response.WriteAsync($"{header.Key}: " +
						$"{header.Value}{Environment.NewLine}");
				}

				await context.Response.WriteAsync(Environment.NewLine);

				// Connection: RemoteIp
				await context.Response.WriteAsync(
					$"Request RemoteIp: {context.Connection.RemoteIpAddress}");
			});
		}
	}
}
